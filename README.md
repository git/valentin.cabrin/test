[![Build Status](https://codefirst.ddns.net/api/badges/valentin.cabrin/test/status.svg)](https://codefirst.ddns.net/valentin.cabrin/test)  
[![Quality Gate Status](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=alert_status)](https://codefirst.ddns.net/sonar/dashboard?id=test)
[![Bugs](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=bugs)](https://codefirst.ddns.net/sonar/dashboard?id=test)
[![Code Smells](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=code_smells)](https://codefirst.ddns.net/sonar/dashboard?id=test)
[![Coverage](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=coverage)](https://codefirst.ddns.net/sonar/dashboard?id=test)  
[![Duplicated Lines (%)](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=duplicated_lines_density)](https://codefirst.ddns.net/sonar/dashboard?id=test)
[![Lines of Code](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=ncloc)](https://codefirst.ddns.net/sonar/dashboard?id=test)
[![Maintainability Rating](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=sqale_rating)](https://codefirst.ddns.net/sonar/dashboard?id=test)
[![Reliability Rating](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=reliability_rating)](https://codefirst.ddns.net/sonar/dashboard?id=test)  
[![Security Rating](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=security_rating)](https://codefirst.ddns.net/sonar/dashboard?id=test)
[![Technical Debt](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=sqale_index)](https://codefirst.ddns.net/sonar/dashboard?id=test)
[![Vulnerabilities](https://codefirst.ddns.net/sonar/api/project_badges/measure?project=test&metric=vulnerabilities)](https://codefirst.ddns.net/sonar/dashboard?id=test)  


# test

Welcome on the test project!  

  

_Generated with a_ **Code#0** _template_  
<img src="Documentation/doc_images/CodeFirst.png" height=40/>   